Inici
=====

Què és Block Valley?
--------------------
**Block Valley** és un consorci promogut per empreses andorranes que treballen
amb tecnologia *blockchain*. L'objectiu del consorci és promoure la tecnologia
*blockchain* al país, creant xarxes permisionades en col·laboració de tots els
integrants del consorci, en benefici del país.

Les xarxes
----------
Actualment, estem creant la primera d'aquestes xarxes. Aquesta xarxa, usarà
*Ethereum* com a plataforma. A la taula de continguts hi trobareu més
informació sobre aquesta.


.. toctree::
   :maxdepth: 3
   :caption: Continguts:
  
   self
   ethereum/index

Indexs i taules
===============

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
