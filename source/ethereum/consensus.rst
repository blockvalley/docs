Consens
=======
El mecanisme de consens en una *blockchain* és el mecanisme que determina què
és requereix per afegir un bloc nou a la *blockchain*. Aquest mecanisme ha
d'assegurar que tots els nodes de la xarxa puguin coindicir de forma
independent quina és la *blockchain* vàlida partint del primer bloc o bloc
*gènesis*, les regles de consens i el mecanisme de consens.

Escollint el mecanisme de consens
---------------------------------
Proof of Work
^^^^^^^^^^^^^
A les xarxes *blockchain* públiques, el mecanisme de consens més conegut és la
prova de treball o **Proof of Work** (PoW_). Aquest mecanisme de consens
permet que tothom pugui participar en l'addició de nous blocs a la
*blockchain*. 

.. _PoW: https://en.bitcoin.it/wiki/Proof_of_work

Per a poder afegir un nou bloc a la *blockchain*, caldrà realitzar còmput
intensiu a fi de trobar la solució a un problema determinat (trobar un *hash*
dins d'un llindar determinat). Aquest còmput intensiu es veu recompensat a 
través de les comisions de les transaccions del bloc afegit i/o noves unitats
de moneda.

Les entitats que dediquen recursos computacionals per afegir nous blocs 
a una *blockchain* s'anomenen miners o en anglès *miners*

Aquest mecanisme permet que sense tenir identitats, es pugui decidir de forma
decentralitzada quin és el següent bloc i per tant, en conjunt, quina és la
*blockchain* vàlida.

.. note::
   A més, els atacants que vulguin censurar transaccions éssent els únics que
   tenen la potestat d'afegir nous blocs a la cadena, hauran de tenir una
   quantitat de recursos excessiva en comparació amb la resta de miners.


Proof of Authority
^^^^^^^^^^^^^^^^^^
A les xarxes permisionades, usar la prova de treball o **Proof of Work**
(PoW_) per a decidir qui serà l'encarregat de crear el següent bloc de
transaccions, no és aconsellable per la seves conseqüències en la seguretat
d'aquesta.

Bàsicament, un atacant amb recursos suficients podria reescriure tota la
*blockchain*. Si el que volem és que només un cert grup de participants
tinguin aquesta potestat d'afegir blocs a fi d'evitar aquest i altres atacs,
hem d'escollir `un altre mecanisme de consens`__.

.. _pow-validity: https://bitcoin.stackexchange.com/q/72685/42162

__ pow-validity_

Per això, usarem com a mecanisme de consens la prova d'autoritat o 
**Proof of Authority** (PoA), on un conjunt d'identitats seleccionades, seràn
qui podràn afegir blocs a la cadena.

Proof of Authority
------------------
A *Proof of Authority*, en comptes de cercar la solució a un problema que 
requereix de computació intensa, requerim la *signatura digital* d'una de les
identitats aprovades per afegir un nou bloc a la cadena.

D'aquesta manera només les entitats seleccionades podran afegir blocs a la 
cadena. 

Les entitats que afegeixen blocs a la cadena en un *Proof of Authority*,
s'anomenen **segelladors**, o en anglès *sealers*.

Implementació de *Proof of Authority* a *Ethereum*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
La plataforma d'*Ethereum* no fou pensada per a implementar 
*Proof of Authority*, doncs es va pensar per ésser una xarxa oberta i no una
permisionada. Tot i així, varis desenvolupadors van pensar com fer funcionar
el protocol en xarxes permisionades usant *Proof of Authority*.

Desenvolupadors del client de *Ethereum* en C++ Parity_ van desenvolupar el
protocol de *Proof of Authority* anomenat Aura_, mentre que els
desenvolupadors del client d'*Ethereum* en Go `Go Ethereum`_ o ``geth`` van
desenvolupar el protocol de *Proof of Authority* anomenat Clique_.

.. _Parity: https://github.com/paritytech/parity-ethereum
.. _Aura: https://wiki.parity.io/Aura
.. _Go Ethereum: https://github.com/ethereum/go-ethereum
.. _Clique: https://github.com/ethereum/EIPs/issues/225

Després d'avaluar ambdos implementacions de mecanismes de consens del tipus
*Proof of Authority* a *Ethereum*, hem escollit Clique_ per davant de Aura_
per motius d'eficiència i simplicitat a l'hora de la seva implementació. 

.. note::
   La nostra elecció també ve donada pel `resultat d'una investigació`__ que
   justament comparava l'elecció d'aquests mètdoes des del punt de vista de
   sistemes distribuïts

   .. _pbft_poa: https://eprints.soton.ac.uk/415083/2/itasec18_main.pdf

   __ _pbft_poa

.. _clique-consensus:

*Clique* com a *Proof of Authority*
-----------------------------------
La implementació de Clique_ permet de forma senzilla evitar també abusos
d'autoritat per part dels segelladors o *sealers*.

La implementació funciona de la següent manera:

S'escull una llista de segelladors al principi de la *blockchain*. Cada
segellador s'identifica per la seva adreça d'*Ethereum* derivada a partir de
las seva clau privada. S'ordena la llista de forma lexicogràfica.

El primer segellador de la llista serà l'encarregat de minar en primer lloc,
el segon en segon lloc, i així per tants segelladors com hi hagi.

En cas que algun segellador no hi sigui a la xarxa quan sigui el seu torn,
el següent segellador prendrà el seu torn després d'un temps d'espera. Si 
aquest tampoc hi és, el torn serà cedit al següent i així consecutivament.

En el cas de que dos segelladors votin alhora, només s'agafarà el bloc
d'aquell segellador més proper al segellador del torn actual.

Per evitar abusos d'autoritat, un segellador només podrà segellar N/2 blocs
seguits. On N és el nombre de segelladors.

.. note::
   Clique_ no sols l'implementa el client `Go Ethereum`_ sinó també Pantheon_
   (implementació en Java d'un client de *Ethereum* amb finalitats d'usar-se
   en xarxes permisionades)

   .. _Pantheon: https://github.com/PegaSysEng/pantheon

   Pantheon_ està implementat per PegaSys_, un equip de ConsenSys_ dedicat a 
   blockchains empresarials.

   .. _PegaSys: https://pegasys.tech
   .. _ConsenSys: https://consensys.net/
