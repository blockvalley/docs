Visió global del funcionament
=============================

.. image:: ../../img/overview.png

Accés a la xarxa
----------------
El primer que succeeïx quan un nou node (sigui regular o segellador), es vol
connectar a la xarxa és cercar on connectar-s'hi. Al éssser una xarxa P2P, 
no hi ha un servidor central, hem de cercar els altres nodes o *peers*. 

Per a fer-ho, demana als *bootnodes* existents, que seria la única figura
central disponible coneguda, quins participants hi ha en aquesta per a
connectar-se a ells. 

Al fer-ho, els *bootnodes* s'apunten que aquest nou node acaba de formar part
de la xarxa, per indicar als futurs nodes que vinguin l'adreça d'aquest junt
amb la dels anteriors.

Sincronització de la xarxa
--------------------------
Un cop el node s'ha unit a la xarxa, comença a demanar blocs des de l'últim
punt on s'hi va quedar. Si és un nou node, aquest punt serà el bloc 0, el bloc
gènesis, on es defineix el començament de tota *blockchain*.

No només descarregarà una còpia de la *blockchain*, sinó que també comprovarà
que els blocs s'hagin signat pels segelladors autoritzats, i les transaccions
siguin vàlides. D'aquesta manera permetem la comprovació automàtica i
independent de les dades de la *blockchain*

Ús de la xarxa
--------------
Un cop tenim el node sincronitzat a l'últim bloc disponible, ja podem fer-ne
ús. Per a fer-ho, haurem d'habilitar la interfície JSON/RPC per a fer crides
amb el protocol RPC estàndard de *Ethereum* cap al node.

.. note::
   Les imatges de *Docker* dels nodes ja venen configurades per habilitar la
   interfície RPC per defecte. Només s'ha d'exposar el port 8545.
