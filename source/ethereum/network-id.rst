Identificador de la xarxa
=========================

Per tal de poder sincronitzar la xarxa de *Ethereum* adient, cada xarxa de 
*Ethereum* ha de definir un identificador de xarxa. Aquest és un nombre únic
per cada xarxa que permet que quan sol·licitem informació (blocs, 
transaccions, ...) a altres nodes, aquests ens retornin la informació de la
xarxa que estem interessats.

La xarxa principal de *Ethereum* usa l'identificador ``1``, i els
identificadors ``2`` al ``4`` s'usen per a les xarxes de prova d'*Ethereum* de
*Morden*, *Ropsten* i *Rinkeby* respectivament.

.. note::
   
   Podeu consultar una llista no exhaustiva de identificadors de xarxes de 
   *Ethereum* públiques conegudes:

   https://ethereum.stackexchange.com/q/17051

Noms i identificadors de xarxa a **Block Valley**
-------------------------------------------------
Hem decidit seria bona idea agafar els cims més alts de cada parròquia
d'Andorra i usar-los tant com a nom de xarxa, com per el seu identificador.

El nom de la xarxa serà aleshores el nom del pic, i l'identificador serà 
l'alçada del pic, en metres per sobre del nivell del mar.

.. note::
   L'alçada serà obtinguda de fonts oficials com cartografia.ad_ i 
   s'arrodonirà a l'enter més proper en cas que l'alçada contingui decimals.

   .. _cartografia.ad: https://www.cartografia.ad/mapa-muntanyes-d-andorra

Així doncs, començarem agafant aquella parròquia amb el cim més baix de totes
les parròquies. En el cas que desitgem llançar una nova xarxa amb diferents
paràmetres o regles de consens que no siguin compatibles amb la xarxa anterior,
agafarem el pic següent i així consecutivament.

+-------------------------+-------------------------+-------------------------+
| **Muntanya**            | **Alçada (msnm)**       | **Parròquia**           |
|                         |                         |                         |
| Nom de la xarxa         | Identificador de xarxa  |                         |
+=========================+=========================+=========================+
| enclar                  | 2383                    | Andorra la Vella        |
+-------------------------+-------------------------+-------------------------+
| monturull               | 2754                    | Sant Julià de Llòria    |
+-------------------------+-------------------------+-------------------------+
| pessons                 | 2850                    | Encamp                  |
+-------------------------+-------------------------+-------------------------+
| cabaneta                | 2863                    | Canillo                 |
+-------------------------+-------------------------+-------------------------+
| portella                | 2905                    | Escaldes-Engordany      |
+-------------------------+-------------------------+-------------------------+
| estanyo                 | 2915                    | Ordino                  |
+-------------------------+-------------------------+-------------------------+
| comapedrosa             | 2946                    | La Massana              |
+-------------------------+-------------------------+-------------------------+

.. important::
   La xarxa actual és **enclar**, amb identificador de xarxa **2383**
