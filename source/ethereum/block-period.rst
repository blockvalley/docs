Periode de blocs
================
El periode de blocs determina cada quan s'afegirà un nou bloc a la 
*blockchain*.

A la xarxa de *Ethereum*, amb el sistema de *Proof of Work*, el mecanisme està
configurat per a que la dificultat d'aquest faci que surtin blocs amb un
període de 15 segons. És a dir, un bloc nou cada (aproximadament) 15 segons.

A la nostra xarxa, proposem també seguir amb un període de 15 segons per bloc,
afegint una peculiaritat. Donat que la xarxa és possible no sigui activa lo 
suficient com per afegir un nou bloc de transaccions cada 15 segons, volem
evitar blocs buits.

Per això, proposem que els segelladors només segellin quan detectin
transaccions a la xarxa que han d'ésser validades. D'aquesta manera si en un
espai de temps no hi ha activitat a la xarxa, els segelladors no afegiran nous
blocs a la *blockchain*, reduint així la mida d'aquesta.

.. note::

   Un bloc buit ocupa, en format descomprimit, 606 bytes.
