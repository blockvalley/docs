Ús de la xarxa
==============

Per a usar la xarxa, podem fer-ho de la mateixa forma que a la xarxa principal,
tenint en compte que ens hem de connectar a un node de la xarxa de **Block
Valley** i que canviarà l'identificador de xarxa.

Aqui trobareu tota la informació per poder tenir un node de la xarxa i per
poder usar-la.

.. toctree::
   :maxdepth: 2
   :caption: Continguts:

   docker
   metamask
