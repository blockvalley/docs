==========
Governança
==========

Donat que es vol tenir una xarxa controlada, hem d'establir les regles de
governança. A banda dels paràmetres especificats al document de :doc:`params`,
també haurem de definir altres aspectes, com la distribució de fitxes o 
*tokens*, els requisits per a ésser un node *segellador*, entre d'altres.

.. note::
   Tot el descrit a continuació està subjecte a canvis un cop tots els membres
   de la xarxa hi estem d'acord.

   Per a contactar amb tothom, podeu fer-ho a través del correu
   :email:`info@blockvalley.info`

**************************
Distribució del *software*
**************************
Les xarxes P2P estan formades per nodes, que no són res més que computadors
executant una aplicació / **software específic**. És important per a 
assegurar la estabilitat de la xarxa que aquest *software* no contingui cap
vulnerabilitat o *bug* que suposi un perill a la xarxa.

.. note::
   Aquest és un dels motius pels quals hem decidit usar el client de `Go
   Ethereum`_ (:ref:`entre d'altres <clique-consensus>`).  Aquest és un client
   d'*Ethereum* que s'usa a la xarxa principal `en la gran majoria dels seus
   nodes`__
   
   .. _Go Ethereum: https://github.com/ethereum/go-ethereum
   .. _node-tracker: https://etherscan.io/nodetracker

   __ node-tracker_

   Té una àmplia comunitat de desenvolupadors al projecte *open-source* que 
   fan que el codi sigui auditat a diari, incrementat la seguretat del 
   *software*.

   Per això, evitarem també aplicar canvis sobre el codi de `Go Ethereum`_,
   per a poder mantenir els nodes al xarxa al dia de les actualitzacions de
   seguretat del *software* sense haver d'invertir temps en aplicar canvis
   específics de la nostra xarxa sobre el client de la xarxa principal.

Ara per ara, el *software* usat per tenir un node a la xarxa, sigui del tipus
que sigui (*bootnode*, *sealer* o segellador o un node regular) és exactament
la última `versió estable de Go Ethereum`__. Això sí, **amb una configuració
específica** per a la nostra xarxa (l'especificada a :doc:`params`).

.. _go-ethereum-releases: https://github.com/ethereum/go-ethereum/releases

__ go-ethereum-releases_

Aquesta configuració bàsicament comprèn dues peces principals:
 - Els **bootnodes** o portes d'entrada a la xarxa.
 - El **bloc gènesis** amb les regles de la xarxa. El primer bloc de la cadena.

Per assegurar que tothom és a la mateixa xarxa, ens haurem de posar d'acord
tots en quins són aquests dos punts. Sobretot, en quin és el **bloc gènesis**
doncs sinó, ens trobarem en diferents cadenes i no podrem tenir una xarxa
uniforme on tothom compartim la mateixa informació.

Estat actual
============

El braç tècnic de `BTC Assessors`_ (`BTCA Labs`_) publica aquestes 
configuracions per a la xarxa de proves **enclar** al repositori de la
organització principal

https://gitlab.com/blockvalley/blockvalley

.. note::
   Tenir una còpia del repositori a altres servidors de *Git* seria una bona
   praxis per evitar que nous membres no puguin accedir a la xarxa si es 
   produeix una caiguda al servei de *Git* de *GitLab*.

   Tenim un mirall del repositori a *GitHub*.
   https://github.com/blockvalley/blockvalley

.. _BTC Assessors: https://www.btcassessors.com
.. _BTCA Labs: https://btcalabs.tech

La última configuració disponible sempre es trobarà a la branca **master**.

.. important::
   Els *commits* hauràn d'estar signats digitalment per un dels dos
   desenvolupadors següents:

   - `D214 E5C6 6C18 EEC5 8B21  11CE 1441 FA43 5DE6 AC64`__
   
   .. _0x5DE6AC64: https://keyserver.ubuntu.com/pks/lookup?search=0x5DE6AC64&op=vindex
   __ 0x5DE6AC64_

     David LJ <:email:`david.lozano@btcassessors.com`>

   - `CB79 2CB0 153C 4121 72B6  144C 37FA 2688 86E3 BB13`__

   .. _0x86E3BB13: https://keyserver.ubuntu.com/pks/lookup?search=0x86E3BB13&op=vindex
   __ 0x86E3BB13_

     Carlos GC <:email:`carlos.cebrecos@btcassessors.com`>

   Així mateix, aquesta documentació on es fa referència a aquestes claus
   també està signada digitalment (els *commits*) al seu repositori de 
   *GitLab* (https://gitlab.com/blockvalley/docs) per una de les claus
   anteriors.

Per a fer-ho més fàcil, es publiquen també imatges de *Docker* al registre
oficial amb aquestes configuracions incloses. Per a més informació sobre el 
seu ús, consulteu el document de :doc:`docker`.

.. warning::
   Les imatges de *Docker* encara no estàn signades digitalment per una de les
   claus anteriors (ni per cap altra).

***************************
Portes d'entrada a la xarxa
***************************

Un punt crític de la xarxa és la porta d'entrada. Quan un nou node vol formar
part de la xarxa, ha de consultar a un o més nodes predefinits on són la resta
de nodes. Sino, cadascú hauria de preguntar a algun membre de la xarxa quin
és el seu node per entrar-hi a formar-ne part, afegint fricció al procés 
d'entrar a la xarxa.

Estat actual
============

Actualment, les dues portes d'entrada, una a Andorra la Vella i l'altre a 
França, es troben gestionades per `BTCA Labs`_ i configurades per defecte a 
les imatges de *Docker* distribuïdes.

.. _governance-sealers:

***********
Segelladors
***********

Els nodes *segelladors* s'encarreguen de decidir en cas d'empat, quina és la
transacció que entrarà a un bloc si s'emeten dos transaccions del mateix origen
amb diferents destinacions (*double spend*).

.. tip::
   Per a més informació, consulteu el document de :doc:`consensus` i tipus de
   nodes :doc:`operation`

Estat actual
============

Hi ha dos segelladors, un a Andorra i l'altre a França, ambdós gestionats per
`BTCA Labs`_. 

Les adreces d'aquests són (respectivament):

- ``0x5ea1e4d01187347ff9b3bc672f4a628c9c75d007`` (``btca-sealer01``)
- ``0xd9c911ac1e861e9a3db2fbfd77b13911519368ab`` (``btca-sealer02``)

.. note::
   Com veieu (la primera) l'hem buscada_ per a que començi per la paraula 
   *sealer* (``5ea1e4``) en llenguatge `l33t`_

   .. _buscada: https://vanity-eth.tk/
   .. _l33t: https://ca.wikipedia.org/wiki/Leet

Estat futur
===========

Cada node de la xarxa podrà auto esdevenir *segellador* un cop hagi demostrat
el seu compromís amb aquesta. Això significarà, en poques paraules, romandre
connectat a la xarxa un gran percentatge del temps (proper a 24/7) i no 
obstaculitzar-ne el seu funcionament (no participar-ne, emetre blocs invàlids,
...).

***********************
Distribució de *tokens*
***********************

Per motius d'implementació, els *tokens* de la xarxa s'emeten a priori (al 
**bloc gènesis**) i no es poden tornar a generar. 

Estat actual
============

El membre impulsor del consorci `BTCA Labs`_ té en possessió tots els *tokens*
de la xarxa de proves **enclar** a esperes d'aplicar el procediment futur un
cop decidit pels membres del consorci.

.. note::
   A més, els nodes segelladors, quan emetin un nou bloc, rebran com a 
   recompensa les comisions de les transaccions contigudes als blocs que
   emetin.
   
   **La comisió d'una transacció és el** gas_ **comprat per poder executar la
   transacció.**

   .. _gas: https://miethereum.com/ether/gas/

   Això recompensa aquest tipus de nodes per la tasca de mantenir un node
   operatiu a la xarxa amb alta disponibilitat.

Estat futur
===========

Els *tokens* es distribueixen als usuaris de la xarxa en funció de la seva 
participació (tenir un node connectat, usar la xarxa de forma correcta, ...).

Per a assegurar-ne la justa distribució en funció d'aquests paràmetres a
definir usarem *smart contracts* a la pròpia *blockchain* on s'hauràn de 
repartir.
