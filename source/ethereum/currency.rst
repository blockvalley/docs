Moneda de la xarxa
==================
A *Ethereum*, existeix una moneda nativa, els *ethers*, que s'usen com a
moneda per transmetre valor i com a mètode de pagament de les comisions de
cada transacció de la xarxa.

Denominació
-----------
A **Block Valley**, també tindrem aquesta moneda nativa, que pasarem a
anomenar **Block Valleys Andorrans** (BVA)

.. note::
   Les subunitats s'anomenaran igual que les de *Ethereum*

   http://ethdocs.org/en/latest/ether.html

 
Emissió de moneda
-----------------
Donat el mecanisme de consens de *Proof of Authority*, no cal incentivar els
segelladors amb nova moneda. Els segelladors voluntariament participen en la
xarxa. Per tant, cada bloc que es generi no generarà noves unitats de moneda,
a diferència de la xarxa principal de *Ethereum*

.. important::
   L'emissió de moneda, doncs, és fixada, un cop, i a l'inici de la xarxa.

   Al primer bloc, l'anomenat bloc gènesis o *genesis block* es precarreguen
   un donat nombre de comptes amb un donat nombre de BVAs.

Ús de la moneda
---------------
La moneda, igual que els *ethers* a tota xarxa *Ethereum*, serveix no només
per transferir valor si aquesta en té, sinó també per pagar les comisions de
les transaccions emeses a la xarxa.

En el cas de **Block Valley**, la moneda ens servirà per controlar l'ús que
s'en fa de la *blockchain*:

- Tal i com s'ha descrit a l'apartat anterior, a l'inici de la *blockchain*
  es precarreguen uns determinats comptes amb un determinat nombre de
  *BVAs*

- En el moment que algú vulgui participar a la xarxa, haurà de sol·licitar
  *BVAs* per poder moure aquests *BVAs* a un altre usuari o per crear 
  contractes intel·ligents o *smart contracts*.

- Sense *BVAs* ningú pot participar activament a la xarxa, doncs no podrà 
  pagar les comissions de les transaccions que cal fer per participar-hi.

Per tant, a través de la gestió dels comptes inicials de *BVAs*, podrem 
controlar qui són de la xarxa. Per exemple, podrem requerir que abans d'obtenir
*BVAs* calgui demostrar l'identitat de l'usuari.

D'aquesta manera evitem fraus alhora d'obtenir *BVAs* i en podem contrar el
seu ús indegut.

.. note::
   Ara per ara, els *BVAs* de la primera xarxa de proves, **enclar**, estàn
   en un compte a expenses de decidir la política d'ús de la xarxa.
