Xarxa permisionada *Ethereum*
=============================

Donada la gran popularitat de la plataforma de *Ethereum*, hem començat per 
formar una xarxa permisionada usant aquesta.

.. toctree::
   :maxdepth: 2
   :caption: Continguts:

   params
   operation
   overview
   usage
   governance
