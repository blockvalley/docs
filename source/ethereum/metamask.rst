Usant MetaMask
==============

Podem usar l'extensió de navegador MetaMask_ per a usar la nostra xarxa en el
nostre navegador d'Internet preferit. Està disponible per a *Google Chrome*,
*Firefox* i *Opera*

.. _MetaMask: https://metamask.io

Un cop instal·lada l'extensió, hem de configurar-lo per a què es connecti a la
xarxa de **Block Valley**, ja que per defecte es connectarà a la xarxa
principal de *Ethereum*.

Configuració de MetaMask
------------------------
Per a obrir la configuració de xarxa, obrim la extensió de **MetaMask** fent
clic sobre la seva icona i **desbloquegem el nostre compte**.

.. _metamask_unlock:

.. figure:: ../../img/metamask_unlock.png
   :align: center
   :scale: 75%

   Pantalla de desbloqueig de MetaMask_

.. note::
   En el cas que no tinguem compte, ens asistirà per crear-ne un. Ens hem de
   desar les (12 o 24) paraules clau del nostre nou compte, doncs seràn les
   que ens permetran accedir-ne a aquest. En cas que les perdem, **no hi haurà
   manera possible de recuperar el compte**.

Un cop tenim accés a la pantalla principal, fem clic sobre el seleccionable de
xarxes per poder afegir-ne una de nova, la de **Block Valley**.

.. hint::
   El seleccionable de xarxes el podeu desplegar fent clic sobre la secció
   marcada en blau a la :ref:`figura anterior <metamask_unlock>`.

Al desplegable, seleccionem la opció **Custom RPC**

.. figure:: ../../img/metamask_networks.png
   :align: center
   :scale: 90%

   Desplegable de selecció de xarxes de MetaMask_

.. important::
   Si no hem **desbloquejat el nostre compte** (introduït la contrasenya), no
   ens apareixerà la següent pantalla.

Ens desplacem cap a la opció de *New Network*. Allà, introduïm les dades del
node on es conectarà el MetaMask_ per accedir a la xarxa. Només cal emplenar
de forma obligatòria la URL del node en format següent:::

   http://<host>:<port>

On *host* és el host amb el node de la xarxa executant-se i *port*, el port
de la interfície JSON-RPC (per defecte, 8545).

.. figure:: ../../img/metamask_network_enclar.png
   :align: center

   Configuració de la nova xarxa de **Block Valley** a MetaMask_


.. warning::
   Donat que el client que usem per a la xarxa, ``geth``, no implementa
   capa de HTTPs, les consultes cap al node no viatjaran xifrades.

   Això pot ésser un risc de seguretat si el node està exposat a Internet.
   De tota manera, donat que les dades que es consultaràn són públiques a
   la *blockchain* de **Block Valley**, no suposa un greu risc de
   privacitat o seguretat, si bé la millor opció és tenir localment un node
   propi.

.. note::
   Si no tenim cap node, podem usar-ne un públic, com el que apareix a la
   figura. En el cas que en disposem d'un, introduïm les dades d'accés
   (bàsicament només cal *host* i *port*).

Finalment, només hem de seleccionar la xarxa **Block Valley** al desplegable de
xarxes que hem fet servir anteriorment per poder realitzar transaccions en
aquesta xarxa.

Farem servir el node que hem configurat per enviar i rebre informació de
la *blockchain* subjacent.

.. figure:: ../../img/metamask_all_networks.png
   :align: center

   Selecció de la xarxa de **Block Valley** configurada.

.. note::
   A les imatges s'ha especificat l'identificador de la xarxa *enclar* i un
   node públic d'aquesta. La mateixa configuració s'aplica a altres xarxes,
   usant un node connectat a una xarxa diferent i un identificador de xarxa
   diferent (o omentent-lo si el node només és connectat a una sola xarxa)
