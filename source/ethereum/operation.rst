Funcionament de la xarxa
========================

.. hint::
   Abans de llegir com funciona la xarxa, es recomana haver llegit el capítol
   de :doc:`paràmetres de la xarxa <params>`, sobretot si no es té
   experiència en xarxes permisionades


Tipus de nodes
--------------

La xarxa es forma principalment de nodes d'*Ethereum*. Donat que hem escollit
*Proof of Authority* com a mecanisme de consens, tindrem dos tipus de nodes:
els nodes regulars i els nodes segelladors. 

A més, també tindrem els nodes anomenats *bootnodes*, que serviran com a porta
d'entrada per a nous nodes. 

A continuació explicarem la funcionalitat de cadascun dels nodes.

Nodes
^^^^^
Els nodes de la xarxa s'encarreguen de connectar amb altres nodes per
comunicar missatges (blocs i transaccions) i desar una còpia local de la
*blockchain*.

Com hem comentat a l'apartat de :doc:`consensus`, el fet de triar el mecanisme
de consens de :ref:`clique-consensus`, fa que usem en concret el client de
*Ethereum* ``geth`` o `Go Ethereum`_ per als nodes de la xarxa.

.. _Go Ethereum: https://github.com/ethereum/go-ethereum

.. important::
   Usarem `Go Ethereum`_ (``geth``) com a implementació de client de
   *Ethereum* de la nostra xarxa.

Dintre dels nodes de la xarxa, trobem principalment dos tipus de nodes segons
la seva funcionalitat. Els dos tipus de nodes executen el mateix codi, pero
s'usen per a diferents funcions.

Nodes regulars
""""""""""""""
Sincronitzen i validen la *blockchain* localment i participen en la
retransmissió d'aquesta per la xarxa. 

S'usen com a punt d'accés a la *blockchain* pels usuaris que la vulguin usar.

.. important::
   Els **nodes regulars** repliquen la *blockchain*, la validen de forma 
   independent i permeten als usuaris accedir a les dades d'aquesta de forma
   local a través de la còpia local descarregada.

Nodes segelladors o *sealers*
"""""""""""""""""""""""""""""
Realitzen les mateixes funcions que els nodes regulars, però a més, tenen
un compte configurat per a emetre blocs.

Aquest compte ha d'ésser un dels comptes autoritzats per a emetre blocs.

.. important::
   Els **nodes segelladors** o **sealer nodes** realitzen les mateixes
   funcions que els **nodes regulars**, però a més, s'encarreguen d'afegir
   nous blocs a la *blockchain* realitzant signatures digitals un cop
   configurat el compte de segellador.

.. note::
   Els comptes autoritzats per a emetre blocs es defineixen al bloc de gènesis
   però aquests poden ésser modificats a posteriori realitzant votacions per
   afegir o eliminar comptes autoritzats.

Bootnodes
^^^^^^^^^

Els *bootnodes* o nodes d'inici, són nodes reduïts que només tenen funcions de
xarxa i no emmagatzemen cap *blockchain*.

El que proporcionen és una porta d'entrada a la xarxa per a nous nodes, 
indicant-lis on són els actuals participants de la xarxa. 

.. warning::
   Per motius de seguretat, els *bootnodes* requereixen d'un parell de claus
   ECDSA donat que són la porta d'entrada a la xarxa.

.. important::

   Els **bootnodes** no tenen desada la *blockchain*, només fan funció de
   porta d'entrada. Connecten als nous nodes amb els nodes que ja existeixen
   de la xarxa.

   Un node normal, també fa de **bootnode**.
