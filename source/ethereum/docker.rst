##############################
Monta el teu node (amb Docker)
##############################

Per reduir la complexitat de tenir un node, hem preparat imatges de Docker_ el
més configurades possible per a que muntar el teu propi node sigui tan fàcil
com executar una comanda.

.. _Docker: https://docker.com

*******************************
El registre d'imatges de Docker
*******************************

Tenim totes les imatges de Docker publicades a Dockerhub_ sota l'usuari de
l'organització ``blockvalley``

.. _Dockerhub: https://hub.docker.com

https://hub.docker.com/u/blockvalley

****************************
Les etiquetes de les imatges
****************************

Cada imatge té tota la configuració possible per executar amb el mínim esforç
posible cada tipus de node.

Això implica que algunes imatges tenen informació de la xarxa a la qual ens hi
volem connectar. És per això que l'etiqueta d'una imatge que requereixi
configuració de xarxa serà el nom de la xarxa on ens hi volguem connectar.

***********
Les imatges
***********

Actualment, disposem de les següents imatges de Docker_:

Node
====

Un node estàndard, amb funcions típiques d'un node de *blockchain*.
També té la interfície JSON-RPC_ habilitada per a poder usar-lo com a punt
d'accés local a la xarxa.

.. _JSON-RPC: https://github.com/ethereum/wiki/wiki/JSON-RPC

.. note::
   Aquesta imatge conté informació de xarxa, de forma que l'etiqueta que
   s'usi per seleccionar la imatge determinarà a quina xarxa és connecta el
   node.

   **És per aquest motiu que no existeix l'etiqueta `latest`**

   A les comandes següent ens connectarem a la única xarxa disponible,
   la xarxa **enclar**.

.. hint::
   Les imatges dels nodes estan basades en la última versió estable del client
   de *Ethereum* `Go Ethereum`_, que alhora usen la base de `Alpine Linux`_

   .. _Go Ethereum: https://github.com/ethereum/go-ethereum
   .. _Alpine Linux: https://alpinelinux.org/

   https://hub.docker.com/r/ethereum/client-go/

   A data d'avui (|today|), és el client de `Go Ethereum`_ versió 1.8.20

Funcions
--------

- Manté una còpia de la *blockchain*
- Verifica que totes les dades de la *blockchain* siguin correctes
- Transmet totes les dades als *peers* de la xarxa P2P als quals roman
  connectat
- Permet consultar localment dades de la *blockchain*
- Permet interactuar amb la xarxa *blockchain* (per exemple, enviar
  transaccions)

Execució
--------

Per executar la imatge del node, podem usar la següent comanda de Docker_:::

   docker run -p 30303:30303 -p 8545:8545 blockvalley/enclar-node

El port 30303 s'utilitza per a les comunicacions de la xarxa P2P, mentre que el
port 8545 l'usem per connectar-nos amb l'API JSON-RPC_ i interactuar amb la
xarxa (per exemple amb :doc:`MetaMask <metamask>`)

.. attention::

   Cal exposar el **port 30303** a *Docker* per poder connectar-se
   a la xarxa i a més, tenir el port ``30303`` accesible externament des
   d'Internet (*tenir el port 30303 obert*).

   **En cas que no tinguem el port obert, no podrem sincronitzar blocs** i si
   és un node nou, només tindrem accesible el bloc gènesis.

   Això implica **modificar les regles de ports (NAT) del router usat per a la
   connexió a Internet**.

.. note::
   Hem de tenir en compte que si no afegim persistència, un cop eliminem
   el *container* de Docker_, haurem de tornar a sincronitzar. Podem afegir
   persistència usant els `volums de Docker`__::

       docker run -p 30303:30303 -p 8545:8545 \
          --mount type=volume,source=node-enclar,destination=/root/.ethereum
          blockvalley/enclar-node

   .. _docker-volumes: https://docs.docker.com/storage/volumes/

   __ docker-volumes_

   Ara si aturem i esborrem el *container* (per exemple, per usar una imatge
   més actualitzada), podem llançar un nou *container* amb la *blockchain* ja
   sincronitzada usant les dades del volum.

Paràmetres
----------

Podem afegir paràmetres addicionals de `Go Ethereum`_ (``geth``) a l'execució
del node, només cal afegir-los darrere de la imatge. Per exemple, per mostrar
l'ajuda on es mostren tots els paràmetres disponibles:::

   docker run blockvalley/enclar-node -h

.. hint::
   En cas de no especificar cap paràmetre, s'usaran els recomenats per
   defecte, que venen inclosos a la imatge. Aquests inclouen, entre d'altres
   els detalls per connectar-se a la xarxa (*bootnodes*, id de xarxa, ...)

Node segellador o *sealer*
==========================

El node segellador és una extensió del node regular definit al punt anterior.
Aquest node està preparat per a segellar blocs, amb una configuració addicional
que només activa el procés de segellat quan hi ha activitat a la xarxa per
reduir al màxim els blocs buits.

Tot l'anterior explicat al node aplica igual per al node segellador, excepte
les diferències a continuació.

Funcions
--------

Totes les funcions de un node, tal i com s'ha vist al punt anterior més:

- Segellat: emissió de blocs nous realitzant signatures digitals quan hi
  pertoqui (hi ha activitat i és el torn oportú)

Execució
--------

A diferència del node, cal configurar un compte de *Ethereum* al segellador,
que usarem per a signar els nous blocs emesos per nosaltres.

Compte de *Ethereum*
^^^^^^^^^^^^^^^^^^^^

Claus ECDSA
"""""""""""
Per a tenir un compte de *Ethereum*, necessitarem un parell de claus ECDSA.

Per a generar un parell de claus, només necessitarem una clau privada ECDSA.
Amb la clau privada després en podem generar la pública i d'aquí l'adreça
de *Ethereum*.

.. attention::
   Haurem de guardar aquesta clau privada de la forma més segura possible,
   doncs algú amb accés a aquesta podria suplantar la identitat de segellador.

   Això implicaria problemes de **denegació de servei** en cas que més dels
   *n/2* segelladors siguin compromesos.

Generar una clau privada es pot fer de forma fàcil amb un *script* de *shell*
present al `repositori de codi principal`__ ::


   ./scripts/generate_bytes.sh > private_key.txt

__ repo_

L'*script* generarà una clau privada ECDSA de 32 bytes (representada en forma
hexadecimal) usant aleatorietat del sistema operatiu (per defecte
``/dev/urandom``).

.. tip::
   També es poden usar *wallets* jeràrquics deterministes (`HDWallets`_) a
   partir d'una llavor o *seed* o bé amb una frase secreta (*mnemonics
   sentence*) usant el `BIP 39`_ per obtenir un arbre de claus privades.

   .. _HDWallets: https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki
   .. _BIP 39: https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki

   Per a obtenir aquesta frase, podem usar un `generador`_ o inclús
   `usar un dau`_ físic per aconseguir la màxima aleatorietat possible

   .. _generador: https://iancoleman.io/bip39/
   .. _usar un dau: https://github.com/mohrt/dice2mnemonic)

Contrasenya de protecció
""""""""""""""""""""""""

Finalment, haurem de definir una contrasenya per bloquejar aquest compte.
D'aquesta manera no tindrem la clau privada desada en clar.

Aquesta també la podem generar de forma fàcil amb un *script* de shell:::

   ./scripts/generate_password.sh > password.txt

Amb la clau privada i contrasenya per protegir-la, ja podem preparar el compte
a usar per segellar.

Configurar el compte del segellador
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Per això, ens cal **usar persistència**, per a que si aturem o esborrem el
*container*, seguim tenint el compte configurat per a emetre blocs.

.. important::
   D'altra manera, si el *container* s'atura i s'elimina, haurem de
   sincronitzar la *blockchain* de nou i configurar el compte per a signar els
   blocs de nou.

   Això pot implicar aturades de la xarxa si succeeix a un nombre de
   segelladors superior a *n/2* on *n* és el nombre de segelladors.

   Tècnicament no és necessari, pero a nivell operatiu **és imprescindible**.

Usarem `volums de docker`__ per a persistir dades del *container* que
executem.

__ docker-volumes_

Per a configurar el *volum*, que anomenarem ``sealer-enclar``, fem servir un
script de *shell* disponible al `repositori de codi principal`__ de
**Block Valley**

.. _repo: https://gitlab.com/blockvalley/blockvalley

__ repo_

Executem la següent comanda: ::

   ./scripts/init_account.sh sealer-enclar private_key.txt password.txt \
   > address.txt

Aquest script executarà la comanda d'importació de compte de ``geth`` i desarà
el resultat al volum seleccionat. Com a resultat, si no hi ha cap error,
retorna l'adreça de *Ethereum* del compte importat.

Podem veure aquesta mostrant el fitxer ``address.txt`` que acabem de generar::

   cat address.txt
   0x5ea1e4d01187347ff9b3bc672f4a628c9c75d007

.. important::
   **Anoteu aquesta adreça**, l'haureu d'enviar al consorci per a poder ésser
   un node segellador del consorci un cop la majoria hi estigui d'acord.

   Ara per ara, el procés de convertir-se en segellador és manual.
   Envieu un correu a :email:`Nodes BlockValley <node@blockvalley.info>` amb
   l'assumpte ``Alta sealer``.

   En el futur, el procés serà automàtic (:ref:`governance-sealers`)


Ja hem inicialitzat el volum ``sealer-enclar`` amb la clau privada del fitxer
``private_key.txt`` protegida amb la contrasenya especificada a ``password.txt``.

Ara ja podem llançar el nostre node segellador:::

  docker run -p 30303:30303 \
      --mount type=volume,source=sealer-enclar,destination=/root/.ethereum \
      --env PASSWORD=$(cat password.txt) \
      blockvalley/enclar-sealer

Un cop sincronitzat, el nostre node ja estarà llest per a emetre nous blocs.
Haurem d'esperar, això sí a que sigui assignat com a segellador per majoria de
vots dels segelladors actuals.

Neteja de fitxers
^^^^^^^^^^^^^^^^^

.. sidebar:: Perquè és important

   Hem d'esborrar el fitxer ``private_key.txt`` amb la clau privada per evitar la
   fuga d'aquesta.

   El fitxer de ``password.txt`` no s'ha d'esborrar doncs ens permet usar la
   clau privada al arrencar el node sense haver de manualment introduïr la
   contrasenya de xifrat de la clau privada.

   **Sobretot cal copiar en un lloc segur aquesta informació abans
   d'esborrar-se, doncs si es perd serà irrecuperable**

Per seguretat, copiarem els fitxers amb dades sensibles **a través d'un mètode
segur, com físicament a un dispositiu o via SSH o altres mètodes xifrats** els
fitxers ``private_key.txt`` i ``password.txt``.

.. code-block:: sh

   cp password.txt private_key.txt /media/myusb
   scp password.txt private_key.txt me@myserver:~/safeplace/

.. note::

   Reemplaceu ``/media/myusb`` o ``me@myserver:~/safeplace/`` per una
   destinació segura per a desar els fitxers de la vostra elecció

Necessitem conservar el fitxer *password.txt* per a quan s'hagi de reiniciar
el *container* de Docker_ pugui desbloquejar el compte, però podem eliminar
de forma segura el fitxer ``private_key.txt`` (un cop l'hem copiat)

.. code-block:: sh

   shred -n 10 private_key.txt

En resum
--------
Comandes a executar:

.. code-block:: sh

   # Creació de compte
   ./scripts/generate_bytes.sh > private_key.txt
   ./scripts/generate_password.sh > password.txt
   # Inicialització de volum de Docker amb compte
   ./scripts/init_account.sh sealer-enclar private_key.txt password.txt
   # Execució
   docker run -p 30303:30303 \
      --env PASSWORD=$(cat password.txt) \
      --mount type=volume,source=sealer-enclar,destination=/root/.ethereum \
      blockvalley/enclar-sealer
   # Deseu la clau privada private_key.txt en un lloc segur
   # cp private_key.txt password.txt /media/myusb
   # cp private_key.txt password.txt me@myserver:~/safeplace
   # La contrasenya password.txt també!
   # Eliminem el rastre per seguretat
   shred -n 10 private_key.txt
