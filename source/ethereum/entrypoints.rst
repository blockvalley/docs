Portes d'accés a la xarxa
=========================

Quan un nou node vol connectar-se a una xarxa P2P, necessita conèixer un 
altre node per a entrar-hi.

Per automatitzar aquest pas, les xarxes P2P usualment defineixen portes 
d'entrada, que són servidors que coneixen a alguns nodes de la xarxa P2P i
redirigeixen els nodes nouvinguts als nodes que ells coneixen de manera que
els nodes nouvinguts puguin connectar-se sense haver de demanar per vies
externes l'adreça d'un node existent.

.. important::
   En *Ethereum*, les portes d'accés a la xarxa es denominen **bootnodes**

   S'identifiquen amb una adreça IP, un port i una clau pública ECDSA (aquesta
   última a efectes de validar la seva identitat).

   El format és: ``enode://pubkey@ip:port``. Per exemple:

   ``enode://a979fb57[...]3bec910127f134779fbcb0cb6d3331163c@52.16.188.185:30303``


Portes d'accés a les xarxes principals de *Ethereum*
----------------------------------------------------
A la xarxa principal de *Ethereum*, les portes d'accés a la xarxa venen 
*hard coded* al codi dels clients de *Ethereum*. D'aquesta manera, 
descarregant l'aplicació, podem accedir a les portes d'accés a la xarxa sense
fer cap petició addicional. D'aquesta manera ens assegurem que si hem
verificat correctament l'integritat de l'aplicació descarregada, també hem
verificat l'integritat alhora de les portes d'entrada a la xarxa.

.. note::
   En el cas de la implementació de *Ethereum* de `Go Ethereum`_, les portes
   d'entrada de la xarxa principal és troben definides en les següents línies:

   .. _Go Ethereum: https://github.com/ethereum/go-ethereum/

   https://github.com/ethereum/go-ethereum/blob/v1.8.20/params/bootnodes.go#L19-L31


Portes d'accés a la xarxa de **Block Valley**
---------------------------------------------
Per poder usar un client de *Ethereum* sense modificacions, no inclourem les
portes d'entrada o *bootnodes* *hard-coded* dins els clients.

Per a especificar les portes d'entrada, aleshores, desarem aquests *bootnodes*
al fitxer de configuració de l'aplicació. També es poden especificar a través
de la línia de comandes

.. note::
   A les imatges de *Docker* de **Block Valley**, ja s'ha copiat la
   configuració adient per tal que no calgui especificar els *bootnodes* 
   manualment

.. important::
   Actualment, a la xarxa **enclar** ja hi tenim dos *bootnodes*, un a
   Andorra, a les oficines d'una de les empreses del consorci i l'altra a
   Espanya
