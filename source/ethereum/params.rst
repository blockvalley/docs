Paràmetres de la xarxa
======================

Per a formar la xarxa permisionada, però, hem hagut de escollir uns paràmetres
diferents de la xarxa principal d'*Ethereum*. Bàsicament, els canvis d'aquests
paràmetres són els següents:

+-------------------------+-------------------------+-------------------------+
| Paràmetre               | Xarxa principal         | Block Valley            |
+=========================+=========================+=========================+
| Consens                 | Proof of Work           | Proof of Authority      |
|                         |                         |                         |
|                         | Algorisme *Ethash*      | Implementació Clique_   |
+-------------------------+-------------------------+-------------------------+
| Periode de blocs        | 15 segons               | 0 segons                |
|                         |                         |                         |
|                         | Flux constant           | Només quan hi ha        |
|                         |                         | transaccions            |
+-------------------------+-------------------------+-------------------------+
| Identificador de la     | 1                       | 2383                    |
| xarxa                   |                         |                         |
|                         |                         | Xarxa de proves         |
|                         |                         | *enclar*                |
+-------------------------+-------------------------+-------------------------+
| Portes d'accés a la     | *Hard-coded* als        | Definits als fitxers    |
| xarxa                   | clients de *Ethereum*   | de configuració         |
|                         |                         |                         |
|                         |                         | (i a les imatges de     |
|                         |                         | *Docker*)               |
+-------------------------+-------------------------+-------------------------+
| Moneda                  | Ethers                  | Block Valleys Andorrans |
|                         |                         | (BVA)                   |
|                         | Generació en cada bloc  |                         |
|                         |                         | Generació al gènesis    |
+-------------------------+-------------------------+-------------------------+

.. _Clique: https://github.com/ethereum/EIPs/issues/225

Als següents capítols de la documentació, detallarem la decisió dels paràmetres
de la xarxa **Block Valley**, comparant-los amb la xarxa principal.


.. toctree::
   :maxdepth: 2
   :caption: Continguts:
  
   consensus
   block-period
   network-id
   entrypoints
   currency
